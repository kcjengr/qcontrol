 # QControl

A QtPyVCP based Virtual Control Panel for LinuxCNC

## Installing via pip

Standard install:  

`pip install .`

Development install:  

`pip install -e .`
